# Anwendung

Single-Page Application (AngularJS, MongoDB, ExpressJS)

Anwendung zur  Verwaltung der Verkäufe und Analyse der Umsätze. Geeignet für kleine Unternehmen (z.B. Bringdienst Bistro).
Quellcode - Schaltfläche "Source" in Menu links 

## Demo-Version hier:

https://limitless-retreat-86885.herokuapp.com


## Vorgang

1.	Registrieren
2.	Mit registrierten Daten einloggen
3.	Im Menu „Sortiment“ anklicken und gewünschte Produkt Kategorie anlegen (z.B. Getränke). Es gibt die Option ein entsprechendes Bild hoch zu laden. Angelegte Kategorie kann jeder Zeit bearbeitet werden.
4.	In der Kategorie kann man beliebige Anzahl Produkte eintragen (Kaffee, Tee, Cola und s.w.) mit Preisangabe.
5.	Im Menu „Add order“ kann man Bestellungen ausführen. Mit „Bestellung abschließen“ ist der Gesamtpreis und Gesamtanzaghl der Produkte ansehbar. Nach Bedarf kann die Bestellung korrigiert werden. 
6.	Im Menu „History“ ist die Übersicht aller Bertellungen. Oben rechts kann man den Filter öffnen und anwenden (Suche nach bestimmter Zeitraum oder nach Bestellungs-Nr. )
7.	Im Menu „Analytics“ sind Graphiken vorhanden, anhand deren kann man Verkäufe Tendenzen ansehen. Die Graphiken zeigen sich an, nur wenn in Datenbank Verkäufe von mehreren vergangenen Tagen vorliegen.
8.	Im Menu „Overview“ kann man sich eine Vorstellung über Umsatzdynamik des Vortags im Vergleich zum durchschnittlichen Wert verschaffen.  




